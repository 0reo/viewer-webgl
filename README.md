## itsme viewer-webgl

[Do it live!](http://itsme3d.github.io/viewer-webgl/)

### Building locally
The built JavaScript files are included in `dist/`, but if you'd like to build them yourself, do the following:

- `npm install`
- `npm install -g gulp`
- `gulp`

This will start a watcher which will recompile the `dist/` files whenever the `src/` files change.

### Running
Run `python -m SimpleHTTPServer` in the root project directory, then
navigate to http://localhost:8000

### Blendering Mixamo animations
##### Because Blender is stupid

1. Make sure you have an autorigged Mixamo model with the correct number of bones (25).
2. Find the animation you want and Queue Download as a Collada (.dae), With Skin.
![Mixamo export](site_content/mixamo.png)
3. Start a new, empty Blender file.
4. File -> Import -> Collada.
5. Select your downloaded .dae file.
6. Expand "Armature" in the Scene Explorer and select ZBrush (your mesh may be called something different, just make sure to select the actual mesh).
7. File -> Export -> three.js (if you don't see this, follow the instructions [here](https://github.com/mrdoob/three.js/tree/master/utils/exporters/blender)).
8. Uncheck all Geometry modifiers, including "Apply Modifiers".
9. Set Skeletal Animations to "Pose".
10. Make sure "Embed Animations" and "Indent JSON" are checked.
11. Export! This should result in a .json file that just contains the bone animation data.
