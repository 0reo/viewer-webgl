var gulp = require("gulp")
var babel = require("gulp-babel")
var continuous_concat = require("gulp-continuous-concat")
var concat = require("gulp-concat")
var watch = require("gulp-watch")
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var gzip = require('gulp-gzip');
var shell = require('gulp-shell')

var paths = {
  libs: ['lib/*.js'],
  scripts: ['src/debug.js', 'src/**/!(debug|main)*.js', 'src/main.js'],
  images: ['assets/environments/*.{jpeg,jpg,png}'],
  chrome: ['assets/images/*.png'],
  animations: ['assets/animations/*.json'],
  models: ['assets/environments/*.dae']
};

gulp.task('libs', function () {
  return gulp.src(paths.libs)
    .pipe(gzip({ append: false, gzipOptions: { level: 9 } }))
    .pipe(gulp.dest("dist"))
});

gulp.task('scripts', function () {
  return gulp.src(paths.scripts)
    .pipe(concat("all.js"))
    .pipe(babel())
    .pipe(gulp.dest("lib"))
    .pipe(gzip({ append: false, gzipOptions: { level: 9 } }))
    .pipe(gulp.dest("dist"))
});

gulp.task('scripts-watch', function () {
  return gulp.src(paths.scripts)
    .pipe(watch("src/**/*.js"))
    .pipe(continuous_concat("all.js"))
    .pipe(babel())
    .pipe(gulp.dest("lib"))
    .pipe(gzip({ append: false, gzipOptions: { level: 9 } }))
    .pipe(gulp.dest("dist"))
});

gulp.task('animations', function() {
  return gulp.src(paths.animations)
    .pipe(gzip({ append: false, gzipOptions: { level: 9 } }))
    .pipe(gulp.dest('dist/animations'));
});

gulp.task('models', function() {
  return gulp.src(paths.models)
    .pipe(gzip({ append: false, gzipOptions: { level: 9 } }))
    .pipe(gulp.dest('dist/environment'));
});

gulp.task('images', function() {
  return gulp.src(paths.images)
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('dist/environment'));
});

gulp.task('chrome', function() {
  return gulp.src(paths.chrome)
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
  gulp.watch(paths.scripts);
  gulp.watch(paths.images);
});

gulp.task('aws', shell.task([
  '/Users/pete/viewer-webgl/aws'
]))

gulp.task('watch', ['scripts-watch', 'libs']);
gulp.task('default', ['scripts', 'libs']);
gulp.task('upload', ['scripts', 'libs', 'images', 'chrome', 'animations', 'models', 'aws']);
