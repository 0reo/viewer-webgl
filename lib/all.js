'use strict';

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var ANIM_CACHE = {};

var IMAnimation = (function () {
  function IMAnimation(name) {
    var _this = this;

    _classCallCheck(this, IMAnimation);

    this.name = name;

    var json = ANIM_CACHE[name];
    if (json) {
      this.onLoad(json);
    } else {
      var loader = new THREE.XHRLoader();
      loader.load(root_url + '/animations/' + name + '.json', function (json) {
        json = JSON.parse(json);
        ANIM_CACHE[name] = json;
        _this.onLoad(json);
      });
    }
  }

  _createClass(IMAnimation, [{
    key: 'onLoad',
    value: function onLoad(json) {
      this.anim = new THREE.Animation(viewer.model.mesh, json);

      if (this.playing) this.play();
    }
  }, {
    key: 'play',
    value: function play() {
      this.playing = true;

      if (this.anim) {
        if (viewer.model.mesh.skeleton.bones.length !== this.anim.hierarchy.length) return console.error('Bones in animation do not match model.');

        this.anim.play();
      }
    }
  }, {
    key: 'stop',
    value: function stop() {
      if (this.anim) this.anim.stop();
    }
  }]);

  return IMAnimation;
})();

var MODEL_SCALE = 70;

var MATERIAL_TYPES = {
  'default': THREE.MeshLambertMaterial,
  unlit: THREE.MeshBasicMaterial,
  phong: THREE.MeshPhongMaterial
};

var IMModel = (function (_THREE$Object3D) {
  _inherits(IMModel, _THREE$Object3D);

  function IMModel(url, animation) {
    var _this2 = this;

    _classCallCheck(this, IMModel);

    _get(Object.getPrototypeOf(IMModel.prototype), 'constructor', this).call(this);

    if (url) {
      this.load(url + '/avatar.json', url + '/avatar.jpg');
    }

    this.anim = {};

    if (animation) {
      this.currentAnimation = animation;
    } else this.currentAnimation = null;

    this.materialType = 'default';

    if (viewer.gui) {
      viewer.gui.add(this, 'materialType', Object.keys(MATERIAL_TYPES)).onFinishChange(function (value) {
        _this2.materialType = value;
      });
    }

    this.scale.set(MODEL_SCALE, MODEL_SCALE, MODEL_SCALE);
  }

  _createClass(IMModel, [{
    key: 'load',
    value: function load(objPath, texPath) {
      THREE.ImageUtils.crossOrigin = 'anonymous';
      this.texture = THREE.ImageUtils.loadTexture(texPath);

      var loader = new THREE.JSONLoader();
      loader.load(objPath, this.onLoadCompleted.bind(this), this.onLoadProgress, this.onLoadFailed);
    }
  }, {
    key: 'initializeObject',
    value: function initializeObject() {
      this.mesh = new THREE.SkinnedMesh(this.obj, this.material);
      this.mesh.castShadow = true;
      this.mesh.frustumCulled = false;
      if (this.currentAnimation) this.addAnimation(this.currentAnimation);

      this.add(this.mesh);
    }
  }, {
    key: 'addAnimation',
    value: function addAnimation(_animation) {
      this.anim[_animation] = new IMAnimation(_animation);
    }
  }, {
    key: 'playCurrentAnimation',
    value: function playCurrentAnimation() {
      if (this.currentAnimation) {
        if (!this.anim[this.currentAnimation]) {
          this.addAnimation(this.currentAnimation);
        }
        this.anim[this.currentAnimation].play();
        console.log('playing ' + this.currentAnimation);
      } else if (Object.keys(this.anim).length) {
        this.currentAnimation = Object.keys(this.anim)[0];
        this.playCurrentAnimation();
      } else console.log('no animation to play');
    }
  }, {
    key: 'playAnimation',
    value: function playAnimation(_animation) {
      if (this.currentAnimation) this.anim[this.currentAnimation].stop();
      this.currentAnimation = _animation;
      this.playCurrentAnimation();
    }
  }, {
    key: 'onLoadCompleted',
    value: function onLoadCompleted(obj) {
      this.obj = obj;
      this.initializeObject();

      this.dispatchEvent({ type: 'load' });
    }
  }, {
    key: 'onLoadProgress',
    value: function onLoadProgress(xhr) {
      if (!xhr.lengthComputable) return;

      var percentComplete = xhr.loaded / xhr.total * 100;
      console.log(Math.round(percentComplete, 2) + '% downloaded');
    }
  }, {
    key: 'onLoadFailed',
    value: function onLoadFailed(xhr) {
      console.error(xhr);
    }
  }, {
    key: 'materialType',
    get: function get() {
      return this._materialType;
    },
    set: function set(type) {
      if (type === this._materialType) return;
      this._materialType = type;

      var material = new MATERIAL_TYPES[type]();
      material.skinning = true;
      material.map = this.texture;
      material.side = THREE.DoubleSide;
      material.shininess = 1;

      this.material = material;

      if (this.mesh) {
        this.mesh.material = material;
      }
    }
  }]);

  return IMModel;
})(THREE.Object3D);

var OBJ_REGEX = /\.obj/;
var MTL_REGEX = /\.mtl/;
var TEXTURE_REGEX = /\.(jpg|jpeg|png|gif)/;

var IMViewer = (function () {
  function IMViewer(debug) {
    var _this3 = this;

    _classCallCheck(this, IMViewer);

    if (!Detector.webgl) return Detector.addGetWebGLMessage();

    this.needsRender = [];
    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.renderer.setClearColor(0x000000);
    this.originalRender = this.renderer.render;
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFShadowMap;
    this.renderer.gammaInput = true;
    this.renderer.gammaOutput = true;

    this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000);
    this.camera.position.set(0, 55, 150);

    this.camera.listener = new THREE.AudioListener();
    this.camera.add(this.camera.listener);

    this.lights = new THREE.Object3D();

    var light = new THREE.DirectionalLight(0xebf3ff, 1.6);
    light.position.set(0, 220, 200);
    light.castShadow = true;
    light.shadowMapWidth = 2048;
    light.shadowMapHeight = 2048;
    light.shadowBias = 0.0001;
    light.shadowDarkness = 0.4;

    var d = 250;
    light.shadowCameraLeft = -d;
    light.shadowCameraRight = d;
    light.shadowCameraTop = d;
    light.shadowCameraBottom = -d;
    light.shadowCameraFar = 1000;
    this.lights.add(light);

    var light = new THREE.DirectionalLight(0xffffff, 1);
    light.position.set(0, -1, 0);
    this.lights.add(light);

    var container = document.querySelector('#renderer');
    container.appendChild(this.renderer.domElement);

    if (debug) {
      this.stats = new Stats();
      container.appendChild(this.stats.domElement);
    }

    this.oControls = new THREE.OrbitControls(this.camera);

    this.fakeCamera = new THREE.Object3D();
    this.controls = new THREE.VRControls(this.fakeCamera);
    this.vreffect = new THREE.VREffect(this.renderer);
    this.vreffect.setSize(window.innerWidth, window.innerHeight);

    this.vrmanager = new WebVRManager(this.renderer, this.vreffect, { hideButton: false });
    var muteButton = this.vrmanager.button.createButton();
    muteButton.src = root_url + '/player/mute.png';
    muteButton.title = 'Mute';
    muteButton.style.bottom = 0;
    muteButton.style.left = 0;
    muteButton.style.display = 'block';
    muteButton.addEventListener('click', this.toggleMute.bind(this));
    document.body.appendChild(muteButton);

    // var sceneButton = this.vrmanager.button.createButton()
    // sceneButton.src = root_url + '/player/scene.png'
    // sceneButton.title = 'Switch scene'
    // sceneButton.style.bottom = 0
    // sceneButton.style.left = '48px'
    // sceneButton.style.display = 'block'
    // sceneButton.addEventListener('click', this.switchScene.bind(this))

    document.addEventListener('keypress', this.moveCityHall.bind(this));
    document.addEventListener('mousemove', this.moveCityHall.bind(this));
    document.addEventListener('mousewheel', this.moveCityHall.bind(this));

    // document.body.appendChild(sceneButton)

    var orig = this.vrmanager.onVRClick_;
    this.vrmanager.onVRClick_ = function () {
      _this3.renderer.shadowMap.enabled = false;
      orig.call(_this3.vrmanager);
    };
  }

  _createClass(IMViewer, [{
    key: 'setScene',
    value: function setScene(scene) {
      if (this.scene) {
        this.needsRender.splice(this.needsRender.indexOf(this.scene), 1);
      }

      this.scene = scene;

      if (this.scene) {
        this.needsRender.push(scene);
      }
    }
  }, {
    key: 'switchScene',
    value: function switchScene() {
      this.setScene(new IMCityScene());
    }
  }, {
    key: 'update',
    value: function update(timestamp, delta) {
      if (this.stats) this.stats.update();

      this.oControls.update();
      //this.controls.update()

      THREE.AnimationHandler.update(delta);
    }
  }, {
    key: 'render',
    value: function render(timestamp) {
      var hijackedRender = this.renderer.render;
      this.renderer.render = this.originalRender;
      for (var i = 0, length = this.needsRender.length; i < length; i++) {
        this.needsRender[i].render();
      }
      this.renderer.render = hijackedRender;

      this.vrmanager.render(this.scene, this.camera, timestamp);

      var oPos = this.camera.position.clone();

      // Apply the VR HMD camera position and rotation
      // on top of the orbited camera.
      var rotatedPosition = this.fakeCamera.position.applyQuaternion(this.camera.quaternion);
      this.camera.position.add(rotatedPosition);
      this.camera.quaternion.multiply(this.fakeCamera.quaternion);

      this.vrmanager.render(this.scene, this.camera, timestamp);

      // Restore the orbit position, so that the OrbitControls can
      // pickup where it left off.
      this.camera.position.copy(oPos);
    }
  }, {
    key: 'onResize',
    value: function onResize() {
      this.camera.aspect = window.innerWidth / window.innerHeight;
      this.camera.updateProjectionMatrix();

      this.vreffect.setSize(window.innerWidth, window.innerHeight);
      this.render();
    }
  }, {
    key: 'playAudio',
    value: function playAudio(file, options) {
      options = options || {};
      console.log('playing');
      var audio = new THREE.Audio(this.camera.listener);
      audio.load(root_url + '/sounds/' + file);
      audio.setVolume(options.volume || 25);

      if (document.cookie == 'mute') audio.autoplay = false;else audio.autoplay = true;

      audio.startTime = options.start || 0;

      if (options.ended) {
        var origOnEnded = audio.source.onended;
        audio.source.onended = function () {
          options.ended.call(audio);
          origOnEnded();
        };
      }

      return this.playing = audio;
    }
  }, {
    key: 'toggleMute',
    value: function toggleMute() {
      if (!this.playing) return;
      if (this.playing.isPlaying) {
        this.playing.pause();
        document.cookie = 'mute; expires=1 Jan 2050 0:00:00 UTC; path=/';
      } else {
        this.playing.play();
        document.cookie = 'mute; expires=1 Jan 2000 0:00:00 UTC; path=/';
      }
    }
  }, {
    key: 'moveCityHall',
    value: function moveCityHall(oToCheckField, oKeyEvent) {

      //var city = this.model;
      //console.log(this.camera.position, this.camera.rotation);
      var city = this.scene.getObjectByName('skybox');

      //var city = this.lights;//this.scene.getObjectByName("city_hall");
      var movement = 5;
      if (oToCheckField.charCode == 56) //+
        {
          city.translateX(+movement);
          city.updateMatrix();
          console.log(city.position);
        } else if (oToCheckField.charCode == 50) // -
        {
          city.translateX(-movement);
          city.updateMatrix();
          console.log(city.position);
        }
      if (oToCheckField.charCode == 52) //+
        {
          city.translateY(+movement);
          city.updateMatrix();
          console.log(city.position);
        } else if (oToCheckField.charCode == 54) // -
        {
          city.translateY(-movement);
          city.updateMatrix();
          console.log(city.position);
        }
      if (oToCheckField.charCode == 43) //+
        {
          city.translateZ(+movement);
          /*city.scale.x += movement;
          city.scale.y += movement;
          city.scale.z += movement;*/
          city.updateMatrix();
          console.log(city.position);
        } else if (oToCheckField.charCode == 45) // -
        {
          city.translateZ(-movement);
          /*city.scale.x -= 1;
          city.scale.y -= 1;
          city.scale.z -= 1;*/
          city.updateMatrix();
          console.log(city.position);
        }
    }
  }]);

  return IMViewer;
})();

var IMCityScene = (function (_THREE$Scene) {
  _inherits(IMCityScene, _THREE$Scene);

  function IMCityScene() {
    _classCallCheck(this, IMCityScene);

    _get(Object.getPrototypeOf(IMCityScene.prototype), 'constructor', this).call(this);

    this.constructStage();
    this.constructScene();

    viewer.model.addEventListener('load', this.initialize.bind(this));
  }

  _createClass(IMCityScene, [{
    key: 'constructStage',
    value: function constructStage() {
      var loader = new THREE.ColladaLoader();
      //loader.crossOrigin = 'anonymous';
      loader.load(root_url + '/environments/city_hall/toronto_6v.dae', this.loadCompleted.bind(this));
    }
  }, {
    key: 'loadCompleted',
    value: function loadCompleted(geometry) {
      this.obj = geometry;
      this.initializeObject();
    }
  }, {
    key: 'initializeObject',
    value: function initializeObject() {
      this.scene = this.obj.scene;

      this.obj.scene.traverse(function (child) {

        if (child instanceof THREE.Mesh) {
          child.castShadow = true;
          child.receiveShadow = true;
          //child.material.needsUpdate = true;
        }
      });

      this.scene.name = 'city_hall';
      this.scene.scale.set(2.3, 2.3, 2.3);
      this.scene.rotation.y = -1.5708 / 2;
      this.x = 320; //500;
      this.y = -290;
      this.z = -1325;
      this.scene.position.set(this.x, this.y, this.z);
      this.scene.getObjectByName('drapes').children[0].material.side = THREE.BackSide;
      this.scene.getObjectByName('stage_mirrors').children[0].material.side = THREE.BackSide;
      //this.scene.getObjectByName("tree1").children[0].material.side = THREE.DoubleSide;
      this.scene.getObjectByName('tree1').children[0].material.depthWrite = false;
      this.scene.getObjectByName('tree1').children[0].material.opacity = 1;
      //this.scene.getObjectByName("tree2").children[0].material.side = THREE.DoubleSide;
      this.scene.getObjectByName('tree2').children[0].material.depthWrite = false;
      this.scene.getObjectByName('tree2').children[0].material.opacity = 1;
      this.scene.getObjectByName('HT_shell').children[0].material.side = THREE.DoubleSide;

      //this.scene.matrixAutoUpdate = false;
      this.scene.updateMatrix();

      this.add(this.scene);
    }
  }, {
    key: 'constructScene',
    value: function constructScene() {

      this.add(new THREE.AmbientLight(0x222223));
      this.add(viewer.lights);

      //    viewer.lights.children[0].position.set(0, 220, -200)
      viewer.lights.children[0].castShadow = false;
      //     viewer.lights.children[0].shadowCameraVisible = true;
      //     viewer.lights.children[0].shadowMapWidth = 2048
      //     viewer.lights.children[0].shadowMapHeight = 2048
      //     viewer.lights.children[0].shadowDarkness = 0.8
      //     viewer.lights.children[0].shadowCameraNear = 500
      //     viewer.lights.children[0].shadowCameraFar = 10000

      var d = 8000;
      viewer.lights.children[1].intensity = 0.9;
      viewer.lights.children[1].position.set(0, 5000, -1200);
      viewer.lights.children[1].castShadow = true;
      viewer.lights.children[1].shadowCameraFar = 7000;
      //viewer.lights.children[1].shadowCameraVisible = true;
      viewer.lights.children[1].shadowCameraLeft = -d;
      viewer.lights.children[1].shadowCameraRight = d;
      viewer.lights.children[1].shadowCameraTop = d;
      viewer.lights.children[1].shadowCameraBottom = -d;
      viewer.lights.children[1].shadowDarkness = 0.75;

      this.add(viewer.lights);

      viewer.model.position.set(-915, -1443, 90);
      viewer.model.rotation.y = -1.5708;
      viewer.model.recieveShadow = true;
      this.add(viewer.model);

      /* var spotlight = new THREE.SpotLight(0xFF0000);
       spotlight.name = "sp1";
       spotlight.castShadow = true;
       spotlight.shadowCameraVisible = true;
       spotlight.position.set(0,0,0);
       spotlight.shadowMapWidth = 64;
       spotlight.shadowMapHeight = 64;
      spotlight.shadowCameraNear = 50;
      spotlight.shadowCameraFar = 150;
      spotlight.shadowCameraFov = 30;
      console.log(viewer.model);
       viewer.model.add( spotlight );	*/

      viewer.camera.near = 0.1;
      viewer.camera.far = 200000;

      //viewer.camera.lookAt(viewer.model.position);

      viewer.camera.updateProjectionMatrix();
      viewer.oControls.target = viewer.model.position;

      viewer.oControls.object.position.copy(viewer.model.position); //(-1225, -1385, 100);
      viewer.oControls.object.position.x = -1225;
      viewer.oControls.object.position.y = -1385;
      viewer.oControls.minDistance = 147.18378;
      viewer.oControls.maxDistance = 1768.203125;
      viewer.oControls.maxPolarAngle = Math.PI / 2;

      viewer.renderer.shadowMap.type = THREE.PCFSoftShadowMap;

      var imagePrefix = root_url + '/environments/city_hall/miramar_';
      var directions = ['lf', 'rt', 'up', 'dn', 'ft', 'bk'];
      var imageSuffix = '.jpg';
      var skySize = 35000;
      var skyGeometry = new THREE.CubeGeometry(skySize, skySize, skySize);

      var materialArray = [];
      for (var i = 0; i < 6; i++) materialArray.push(new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture(imagePrefix + directions[i] + imageSuffix),
        side: THREE.BackSide
      }));
      var skyMaterial = new THREE.MeshFaceMaterial(materialArray);
      var skyBox = new THREE.Mesh(skyGeometry, skyMaterial);
      skyBox.name = 'skybox';
      skyBox.position.set(35, 4895, 775);
      //skyBox.position.set(viewer.model.position);
      this.add(skyBox);
    }
  }, {
    key: 'initialize',
    value: function initialize() {
      if (viewer.model.currentAnimation) viewer.model.playCurrentAnimation();else viewer.model.playAnimation('bored');
    }
  }, {
    key: 'render',
    value: function render() {}
  }]);

  return IMCityScene;
})(THREE.Scene);

var MIRROR_COLOR = 0x778080;

var IMDanceScene = (function (_THREE$Scene2) {
  _inherits(IMDanceScene, _THREE$Scene2);

  function IMDanceScene() {
    _classCallCheck(this, IMDanceScene);

    _get(Object.getPrototypeOf(IMDanceScene.prototype), 'constructor', this).call(this);

    this.constructStage();
    this.constructMirror();
    this.constructScene();

    viewer.model.addEventListener('load', this.initialize.bind(this));
  }

  _createClass(IMDanceScene, [{
    key: 'constructStage',
    value: function constructStage() {
      var texture = THREE.ImageUtils.loadTexture(root_url + '/environments/dance/stage.jpg');
      var stage = new THREE.Mesh(new THREE.BoxGeometry(250, 40, 160), new THREE.MeshPhongMaterial({ map: texture }));

      stage.receiveShadow = true;
      stage.position.z = -30;
      this.add(stage);
    }
  }, {
    key: 'constructMirror',
    value: function constructMirror() {
      this.mirror = new THREE.Mirror(viewer.renderer, viewer.camera, {
        textureWidth: window.innerWidth,
        textureHeight: window.innerHeight,
        color: MIRROR_COLOR
      });

      var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(230, 120), this.mirror.material);

      mesh.position.y = 50;
      mesh.position.z = -100;
      mesh.add(this.mirror);
      this.add(mesh);
    }
  }, {
    key: 'constructScene',
    value: function constructScene() {
      this.add(new THREE.AmbientLight(0x222222));
      this.add(viewer.lights);

      viewer.camera.position.x = -10;
      viewer.model.position.y = 19.9;
      this.add(viewer.model);
    }
  }, {
    key: 'initialize',
    value: function initialize() {
      if (this.animation) {
        var anim = new IMAnimation(this.animation);
      } else {
        var anim = new IMAnimation('breakdancing');
      }
      anim.play();
    }
  }, {
    key: 'render',
    value: function render() {
      this.mirror.render();
    }
  }]);

  return IMDanceScene;
})(THREE.Scene);

;(function () {
  var message = document.querySelector('[data-message]');
  var hint = document.querySelector('.viewer-hint');

  var query = location.search.substr(1).split('&');
  var params = { debug: debug };
  for (var i = 0, count = query.length; i < count; i++) {
    var parts = query[i].split('=');
    params[parts[0]] = parts[1];
  }

  var email = params.email;
  var key = params.key;
  var sound = params.sound;
  var scene = params.scene;

  if (!email || !key) {
    message.innerHTML = 'Error: No model key provided.';
    return;
  }

  if (co) THREE.ImageUtils.crossOrigin = 'anonymous';

  var clock = new THREE.Clock();

  window.viewer = new IMViewer(params.debug);
  if (params.debug) viewer.gui = new dat.GUI();

  viewer.model = new IMModel(root_url + '/users/' + email + '/' + key, params.animation);

  function doScene(scene) {
    if (scene == 'dance') {
      viewer.setScene(new IMDanceScene());
    } else if (scene == 'city_hall') {
      viewer.setScene(new IMCityScene());
    } else {
      message.innerHTML = 'No scene to load, cannot continue.';
    }
  }
  if (scene) {
    doScene(scene);
  } else {
    doScene('city_hall');
  }

  function doSound(sound) {
    if (window.viewer.playing) window.viewer.playing.stop();
    window.viewer.playAudio(sound + '.mp3', { start: 0, volume: 100 });
  }
  if (sound) doSound(sound);

  message.innerHTML = 'Loading model data...';

  viewer.model.addEventListener('load', function () {
    message.innerHTML = '';
    animate();
  });

  function receiveMessage(event) {
    var origin = event.origin || event.originalEvent.origin; // For Chrome, the origin property is in the event.originalEvent object.
    if (origin !== 'http://localhost:8000' && origin.indexOf('www.itsme3d.com') !== -1) return;
    var data = event.data.split(',');
    if (data[0] == 'sound') {
      doSound(data[1]);
    }
    // under contruction
    else if (data[0] == 'animation') {
      viewer.model.playAnimation(data[1]);
    } // else if (data[0] == "scene") {
    //   doScene(data[1])
    // }
    else {
      message.innerHTML = 'Bad control data received, cannot continue.';
    }
  }

  function animate(timestamp) {
    var delta = clock.getDelta();
    requestAnimationFrame(animate);
    viewer.update(timestamp, delta);
    viewer.render(timestamp, delta);
  }
  requestAnimationFrame(animate);

  addEventListeners();

  function addEventListeners() {
    window.addEventListener('resize', viewer.onResize.bind(viewer), false);
    window.addEventListener('message', receiveMessage, false);
  }
})();