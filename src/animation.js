const ANIM_CACHE = {}

class IMAnimation {
  constructor(name) {
    this.name = name

    var json = ANIM_CACHE[name]
    if (json) {
      this.onLoad(json)
    } else {
      var loader = new THREE.XHRLoader
      loader.load(root_url + '/animations/' + name + '.json', (json) => {
        json = JSON.parse(json)
        ANIM_CACHE[name] = json
        this.onLoad(json)
      })
    }
  }

  onLoad(json) {
    this.anim = new THREE.Animation(viewer.model.mesh, json)

    if (this.playing)
      this.play()
  }

  play() {
    this.playing = true

    if (this.anim) {
      if (viewer.model.mesh.skeleton.bones.length !== this.anim.hierarchy.length)
        return console.error("Bones in animation do not match model.")

      this.anim.play()
    }
  }

  stop() {
    if (this.anim)
      this.anim.stop()
  }
}
