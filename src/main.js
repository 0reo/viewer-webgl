;(function() {
  var message = document.querySelector('[data-message]')
  var hint = document.querySelector('.viewer-hint')

  var query = location.search.substr(1).split('&')
  var params = {debug: debug}
  for (let i = 0, count = query.length; i < count; i++) {
    let parts = query[i].split('=')
    params[parts[0]] = parts[1]
  }

  var email = params.email
  var key = params.key
  var sound = params.sound
  var scene = params.scene

  if (!email || !key) {
    message.innerHTML = "Error: No model key provided."
    return
  }

  if (co) THREE.ImageUtils.crossOrigin = "anonymous";

  var clock = new THREE.Clock

  window.viewer = new IMViewer(params.debug)
  if (params.debug) viewer.gui = new dat.GUI()

  viewer.model = new IMModel(`${root_url}/users/${email}/${key}`, params.animation)

  function doScene(scene) {
    if (scene == "dance") {
      viewer.setScene(new IMDanceScene())
    } else if (scene == "city_hall") {
      viewer.setScene(new IMCityScene())
    } else {
      message.innerHTML = "No scene to load, cannot continue."
    }
  }
  if (scene) {
    doScene(scene)
  } else {
    doScene('city_hall')
  }

  function doSound(sound) {
    if (window.viewer.playing) window.viewer.playing.stop();
    window.viewer.playAudio(sound + '.mp3', {start: 0, volume: 100});
  }
  if (sound) doSound(sound)

  message.innerHTML = "Loading model data..."

  viewer.model.addEventListener('load', function() {
    message.innerHTML = ""
    animate()
  })

  function receiveMessage(event)
  {
    var origin = event.origin || event.originalEvent.origin; // For Chrome, the origin property is in the event.originalEvent object.
    if (origin !== "http://localhost:8000" && origin.indexOf('www.itsme3d.com') !== -1)
      return;
    var data = event.data.split(",");
    if (data[0] == "sound") {
      doSound(data[1])
    }
    // under contruction
     else if (data[0] == "animation") {
       viewer.model.playAnimation(data[1]);
     }// else if (data[0] == "scene") {
    //   doScene(data[1])
    // }
    else {
      message.innerHTML = "Bad control data received, cannot continue."
    }
  }

  function animate(timestamp) {
    var delta = clock.getDelta()
    requestAnimationFrame(animate)
    viewer.update(timestamp, delta)
    viewer.render(timestamp, delta)
  }
  requestAnimationFrame(animate)

  addEventListeners()

  function addEventListeners() {
    window.addEventListener('resize', viewer.onResize.bind(viewer), false)
    window.addEventListener('message', receiveMessage, false)
  }

})()
