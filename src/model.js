const MODEL_SCALE = 70

const MATERIAL_TYPES = {
  default: THREE.MeshLambertMaterial,
  unlit: THREE.MeshBasicMaterial,
  phong: THREE.MeshPhongMaterial
}

class IMModel extends THREE.Object3D {
  constructor(url, animation) {
    super()

    if (url) {
      this.load(`${url}/avatar.json`, `${url}/avatar.jpg`)
    }


    this.anim = {};

    if (animation){
      this.currentAnimation = animation;
    } else
      this.currentAnimation = null;


    this.materialType = 'default'

    if (viewer.gui) {
      viewer.gui.add(this, 'materialType', Object.keys(MATERIAL_TYPES))
        .onFinishChange((value) => {
          this.materialType = value
        })
    }

    this.scale.set(MODEL_SCALE, MODEL_SCALE, MODEL_SCALE)
  }

  load(objPath, texPath) {
    THREE.ImageUtils.crossOrigin = 'anonymous'
    this.texture = THREE.ImageUtils.loadTexture(texPath)

    var loader = new THREE.JSONLoader
    loader.load(objPath, this.onLoadCompleted.bind(this), this.onLoadProgress, this.onLoadFailed)
  }

  get materialType() {
    return this._materialType
  }

  set materialType(type) {
    if (type === this._materialType) return
    this._materialType = type

    var material = new MATERIAL_TYPES[type]()
    material.skinning = true
    material.map = this.texture
    material.side = THREE.DoubleSide
    material.shininess = 1

    this.material = material

    if (this.mesh) {
      this.mesh.material = material
    }
  }

  initializeObject() {
    this.mesh = new THREE.SkinnedMesh(this.obj, this.material)
    this.mesh.castShadow = true
    this.mesh.frustumCulled = false
    if(this.currentAnimation)
      this.addAnimation(this.currentAnimation);

    this.add(this.mesh)
  }

  addAnimation(_animation){
      this.anim[_animation] = new IMAnimation(_animation);
  }

  playCurrentAnimation(){
    if (this.currentAnimation){
      if(!this.anim[this.currentAnimation]){
        this.addAnimation(this.currentAnimation);
      }
      this.anim[this.currentAnimation].play();
      console.log("playing "+this.currentAnimation);
    }
    else if( Object.keys(this.anim).length){
      this.currentAnimation = Object.keys(this.anim)[0]
      this.playCurrentAnimation();
    }
    else
      console.log("no animation to play");
  }

  playAnimation(_animation){
    if (this.currentAnimation)
      this.anim[this.currentAnimation].stop();
    this.currentAnimation = _animation;
    this.playCurrentAnimation();

  }

  onLoadCompleted(obj) {
    this.obj = obj
    this.initializeObject()

    this.dispatchEvent({type: 'load'})
  }

  onLoadProgress(xhr) {
    if (!xhr.lengthComputable) return

    var percentComplete = xhr.loaded / xhr.total * 100
    console.log(`${Math.round(percentComplete, 2)}% downloaded`)
  }

  onLoadFailed(xhr) {
    console.error(xhr)
  }
}
