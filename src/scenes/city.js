
class IMCityScene extends THREE.Scene {
  constructor() {
    super()

    this.constructStage()
    this.constructScene()

    viewer.model.addEventListener('load', this.initialize.bind(this))
  }

  constructStage() {
    var loader = new THREE.ColladaLoader();
    //loader.crossOrigin = 'anonymous';
    loader.load(root_url + '/environments/city_hall/toronto_6v.dae', this.loadCompleted.bind(this));
  }

  loadCompleted ( geometry ) {
	this.obj = geometry;
    this.initializeObject();
  }


  initializeObject() {
    this.scene = this.obj.scene;

	this.obj.scene.traverse( function ( child ) {

		if (child instanceof THREE.Mesh) {
			child.castShadow = true;
			child.receiveShadow = true;
			//child.material.needsUpdate = true;
		}
	} );

    this.scene.name = "city_hall";
    this.scene.scale.set( 2.3, 2.3, 2.3 );
	this.scene.rotation.y = -1.5708/2;
    this.x = 320;//500;
    this.y = -290;
    this.z = -1325;
    this.scene.position.set( this.x, this.y, this.z );
    this.scene.getObjectByName("drapes").children[0].material.side = THREE.BackSide;
	this.scene.getObjectByName("stage_mirrors").children[0].material.side = THREE.BackSide;
	//this.scene.getObjectByName("tree1").children[0].material.side = THREE.DoubleSide;
	this.scene.getObjectByName("tree1").children[0].material.depthWrite = false;
	this.scene.getObjectByName("tree1").children[0].material.opacity = 1;
	//this.scene.getObjectByName("tree2").children[0].material.side = THREE.DoubleSide;
	this.scene.getObjectByName("tree2").children[0].material.depthWrite = false;
	this.scene.getObjectByName("tree2").children[0].material.opacity = 1;
	this.scene.getObjectByName("HT_shell").children[0].material.side = THREE.DoubleSide;

    //this.scene.matrixAutoUpdate = false;
    this.scene.updateMatrix();

    this.add(this.scene)
  }


  constructScene() {

    this.add(new THREE.AmbientLight(0x222223))
    this.add(viewer.lights)

//    viewer.lights.children[0].position.set(0, 220, -200)
    viewer.lights.children[0].castShadow = false
//     viewer.lights.children[0].shadowCameraVisible = true;
//     viewer.lights.children[0].shadowMapWidth = 2048
//     viewer.lights.children[0].shadowMapHeight = 2048
//     viewer.lights.children[0].shadowDarkness = 0.8
//     viewer.lights.children[0].shadowCameraNear = 500
//     viewer.lights.children[0].shadowCameraFar = 10000

	var d = 8000;
	viewer.lights.children[1].intensity = 0.9;
    viewer.lights.children[1].position.set(0, 5000, -1200)
    viewer.lights.children[1].castShadow = true
	viewer.lights.children[1].shadowCameraFar = 7000
    //viewer.lights.children[1].shadowCameraVisible = true;
    viewer.lights.children[1].shadowCameraLeft = -d
    viewer.lights.children[1].shadowCameraRight = d
    viewer.lights.children[1].shadowCameraTop = d
    viewer.lights.children[1].shadowCameraBottom = -d
	viewer.lights.children[1].shadowDarkness = 0.75;

    this.add(viewer.lights)


	viewer.model.position.set(-915, -1443, 90);
	viewer.model.rotation.y = -1.5708;
	viewer.model.recieveShadow = true;
    this.add(viewer.model)

   /* var spotlight = new THREE.SpotLight(0xFF0000);
    spotlight.name = "sp1";
    spotlight.castShadow = true;
    spotlight.shadowCameraVisible = true;
    spotlight.position.set(0,0,0);
    spotlight.shadowMapWidth = 64;
    spotlight.shadowMapHeight = 64;
	spotlight.shadowCameraNear = 50;
	spotlight.shadowCameraFar = 150;
	spotlight.shadowCameraFov = 30;
	console.log(viewer.model);
    viewer.model.add( spotlight );	*/

    viewer.camera.near = 0.1;
    viewer.camera.far = 200000;

	//viewer.camera.lookAt(viewer.model.position);

    viewer.camera.updateProjectionMatrix();
	viewer.oControls.target = viewer.model.position;

	viewer.oControls.object.position.copy(viewer.model.position);//(-1225, -1385, 100);
	viewer.oControls.object.position.x = -1225;
	viewer.oControls.object.position.y = -1385;
 	viewer.oControls.minDistance = 147.18378;
 	viewer.oControls.maxDistance = 1768.203125;
 	viewer.oControls.maxPolarAngle = Math.PI / 2;

    viewer.renderer.shadowMap.type = THREE.PCFSoftShadowMap

	var imagePrefix = root_url + "/environments/city_hall/miramar_";
	var directions  = ["lf", "rt", "up", "dn", "ft", "bk"];
	var imageSuffix = ".jpg";
	var skySize = 35000;
	var skyGeometry = new THREE.CubeGeometry( skySize, skySize, skySize );

	var materialArray = [];
	for (var i = 0; i < 6; i++)
		materialArray.push(new THREE.MeshBasicMaterial({
      map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
      side: THREE.BackSide
    }));
	var skyMaterial = new THREE.MeshFaceMaterial( materialArray );
	var skyBox = new THREE.Mesh( skyGeometry, skyMaterial );
	skyBox.name = "skybox";
	skyBox.position.set(35, 4895, 775);
	//skyBox.position.set(viewer.model.position);
	this.add( skyBox );

  }

  initialize() {
  	if (viewer.model.currentAnimation)
    	viewer.model.playCurrentAnimation()
	else
		viewer.model.playAnimation("bored")
  }

  render() {
  }
}
