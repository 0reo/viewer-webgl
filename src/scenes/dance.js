const MIRROR_COLOR = 0x778080

class IMDanceScene extends THREE.Scene {
  constructor() {
    super()

    this.constructStage()
    this.constructMirror()
    this.constructScene()

    viewer.model.addEventListener('load', this.initialize.bind(this))
  }

  constructStage() {
    var texture = THREE.ImageUtils.loadTexture(root_url + '/environments/dance/stage.jpg')
    var stage = new THREE.Mesh(
      new THREE.BoxGeometry(250, 40, 160),
      new THREE.MeshPhongMaterial({map: texture})
    )

    stage.receiveShadow = true
    stage.position.z = -30
    this.add(stage)
  }

  constructMirror() {
    this.mirror = new THREE.Mirror(
      viewer.renderer,
      viewer.camera,
      {
        textureWidth: window.innerWidth,
        textureHeight: window.innerHeight,
        color: MIRROR_COLOR
      }
    )

    var mesh = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(230, 120),
      this.mirror.material
    )

    mesh.position.y = 50
    mesh.position.z = -100
    mesh.add(this.mirror)
    this.add(mesh)
  }

  constructScene() {
    this.add(new THREE.AmbientLight(0x222222))
    this.add(viewer.lights)

    viewer.camera.position.x = -10
    viewer.model.position.y = 19.9
    this.add(viewer.model)
  }

  initialize() {
    if (this.animation) {
      var anim = new IMAnimation(this.animation)
    } else {
      var anim = new IMAnimation('breakdancing')
    }
    anim.play()
  }

  render() {
    this.mirror.render()
  }
}
