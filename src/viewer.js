const OBJ_REGEX = /\.obj/
const MTL_REGEX = /\.mtl/
const TEXTURE_REGEX = /\.(jpg|jpeg|png|gif)/

class IMViewer {
  constructor(debug) {
    if (!Detector.webgl)
      return Detector.addGetWebGLMessage()

    this.needsRender = []
    this.renderer = new THREE.WebGLRenderer({antialias: true})
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.renderer.setSize(window.innerWidth, window.innerHeight)
    this.renderer.setClearColor(0x000000)
    this.originalRender = this.renderer.render
    this.renderer.shadowMap.enabled = true
    this.renderer.shadowMap.type = THREE.PCFShadowMap
    this.renderer.gammaInput = true
    this.renderer.gammaOutput = true

    this.camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 2000)
    this.camera.position.set(0, 55, 150)

    this.camera.listener = new THREE.AudioListener
    this.camera.add(this.camera.listener)

    this.lights = new THREE.Object3D

    var light = new THREE.DirectionalLight(0xebf3ff, 1.6)
    light.position.set(0, 220, 200)
    light.castShadow = true
    light.shadowMapWidth = 2048
    light.shadowMapHeight = 2048
    light.shadowBias = 0.0001
    light.shadowDarkness = 0.4

    var d = 250
    light.shadowCameraLeft = -d
    light.shadowCameraRight = d
    light.shadowCameraTop = d
    light.shadowCameraBottom = -d
    light.shadowCameraFar = 1000
    this.lights.add(light)

    var light = new THREE.DirectionalLight(0xffffff, 1)
    light.position.set(0, -1, 0)
    this.lights.add(light)

    var container = document.querySelector('#renderer')
    container.appendChild(this.renderer.domElement)

    if (debug) {
      this.stats = new Stats()
      container.appendChild(this.stats.domElement)
    }

    this.oControls = new THREE.OrbitControls( this.camera );

    this.fakeCamera = new THREE.Object3D();
    this.controls = new THREE.VRControls(this.fakeCamera)
    this.vreffect = new THREE.VREffect(this.renderer)
    this.vreffect.setSize(window.innerWidth, window.innerHeight)

    this.vrmanager = new WebVRManager(this.renderer, this.vreffect, {hideButton: false})
    var muteButton = this.vrmanager.button.createButton()
    muteButton.src = root_url + '/player/mute.png'
    muteButton.title = 'Mute'
    muteButton.style.bottom = 0
    muteButton.style.left = 0
    muteButton.style.display = 'block'
    muteButton.addEventListener('click', this.toggleMute.bind(this))
    document.body.appendChild(muteButton)

    // var sceneButton = this.vrmanager.button.createButton()
    // sceneButton.src = root_url + '/player/scene.png'
    // sceneButton.title = 'Switch scene'
    // sceneButton.style.bottom = 0
    // sceneButton.style.left = '48px'
    // sceneButton.style.display = 'block'
    // sceneButton.addEventListener('click', this.switchScene.bind(this))



    document.addEventListener("keypress", this.moveCityHall.bind(this))
    document.addEventListener("mousemove", this.moveCityHall.bind(this))
    document.addEventListener("mousewheel", this.moveCityHall.bind(this))

    // document.body.appendChild(sceneButton)

    var orig = this.vrmanager.onVRClick_
    this.vrmanager.onVRClick_ = () => {
      this.renderer.shadowMap.enabled = false
      orig.call(this.vrmanager)
    }
  }

  setScene(scene) {
    if (this.scene) {
      this.needsRender.splice(this.needsRender.indexOf(this.scene), 1)
    }

    this.scene = scene

    if (this.scene) {
      this.needsRender.push(scene)
    }
  }

  switchScene() {
    this.setScene(new IMCityScene())
  }

  update(timestamp, delta) {
    if (this.stats)
      this.stats.update()

    this.oControls.update();
    //this.controls.update()

    THREE.AnimationHandler.update(delta)
  }

  render(timestamp) {
    var hijackedRender = this.renderer.render
    this.renderer.render = this.originalRender
    for (var i = 0, length = this.needsRender.length; i < length; i++) {
      this.needsRender[i].render()
    }
    this.renderer.render = hijackedRender

    this.vrmanager.render(this.scene, this.camera, timestamp)

    var oPos = this.camera.position.clone();

    // Apply the VR HMD camera position and rotation
    // on top of the orbited camera.
    var rotatedPosition = this.fakeCamera.position.applyQuaternion(
      this.camera.quaternion);
    this.camera.position.add(rotatedPosition);
    this.camera.quaternion.multiply(this.fakeCamera.quaternion);

    this.vrmanager.render(this.scene, this.camera, timestamp)


    // Restore the orbit position, so that the OrbitControls can
    // pickup where it left off.
    this.camera.position.copy(oPos);
  }

  onResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight
    this.camera.updateProjectionMatrix()

    this.vreffect.setSize(window.innerWidth, window.innerHeight)
    this.render()
  }

  playAudio(file, options) {
    options = options || {}
console.log("playing");
    var audio = new THREE.Audio(this.camera.listener)
    audio.load(root_url + '/sounds/' + file)
    audio.setVolume(options.volume || 25)

    if (document.cookie == 'mute') audio.autoplay = false
    else audio.autoplay = true

    audio.startTime = options.start || 0

    if (options.ended) {
      var origOnEnded = audio.source.onended
      audio.source.onended = function() {
        options.ended.call(audio)
        origOnEnded()
      }
    }

    return this.playing = audio
  }

  toggleMute() {
    if (!this.playing) return
    if (this.playing.isPlaying) {
      this.playing.pause()
      document.cookie = "mute; expires=1 Jan 2050 0:00:00 UTC; path=/"
    } else {
      this.playing.play()
      document.cookie = "mute; expires=1 Jan 2000 0:00:00 UTC; path=/"
    }
  }


  moveCityHall(oToCheckField, oKeyEvent) {


      //var city = this.model;
      //console.log(this.camera.position, this.camera.rotation);
      var city = this.scene.getObjectByName("skybox");

      //var city = this.lights;//this.scene.getObjectByName("city_hall");
      var movement = 5;
      if(oToCheckField.charCode == 56) //+
      {
        city.translateX( +movement );
        city.updateMatrix();
        console.log(city.position);
      }
      else if(oToCheckField.charCode == 50) // -
      {
        city.translateX( -movement );
        city.updateMatrix();
        console.log(city.position);
      }
      if(oToCheckField.charCode == 52) //+
      {
        city.translateY( +movement );
        city.updateMatrix();
        console.log(city.position);
      }
      else if(oToCheckField.charCode == 54) // -
      {
        city.translateY( -movement );
        city.updateMatrix();
        console.log(city.position);
      }
      if(oToCheckField.charCode == 43) //+
      {
        city.translateZ( +movement );
        /*city.scale.x += movement;
        city.scale.y += movement;
        city.scale.z += movement;*/
        city.updateMatrix();
        console.log(city.position);
      }
      else if(oToCheckField.charCode == 45) // -
      {
        city.translateZ( -movement );
        /*city.scale.x -= 1;
        city.scale.y -= 1;
        city.scale.z -= 1;*/
        city.updateMatrix();
        console.log(city.position);
      }
    }
}
